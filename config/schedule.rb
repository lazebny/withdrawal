set :output, %(#{File.dirname(__FILE__)}/../log/cron.log)

job_type :new_rake, "cd :path && " \
                "HANAMI_ENV=test " \
                "BUNDLE_GEMFILE=/srv/app/Gemfile " \
                "BUNDLE_PATH=/bundle_cache " \
                ":bundle_command rake :task --silent :output"

def start_now_and_every(every:, delay: nil, &block)
  args = {}

  if delay
    args[:at] = (Time.now + delay).strftime('%l:%M%P')
                                  .downcase
  end

  every(every, args, &block)
end

# every 1.minute do
#   new_rake 'show_current_time'
# end


start_now_and_every(every: 1.minute) { new_rake 'show_current_time' }
start_now_and_every(every: 1.minute) { new_rake 'backup_payeer_operations' }
start_now_and_every(every: 3.hour, delay: 90) { new_rake 'green_telecom' }
# start_now_and_every(every: 3.hour, delay: 90) { new_rake 'rosoplata' }
start_now_and_every(every: 3.hour, delay: 90) { new_rake 'storti_investment' }
