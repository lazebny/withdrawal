# coding: UTF-8
describe Withdrawal::Workers::RosoplataWorker, :feature, :js do
  subject { described_class }

  before { Withdrawal::Helpers::CapybaraHelper.setup(use_proxy: false) }

  it { described_class.perform }
end
