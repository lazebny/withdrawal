module Web::Views::PayeerAccounts
  class Index
    include Web::View

    def payeer_accounts
      ConfigHelper.section_in_symbols(:payeer_accounts)
                  .each_with_index
                  .map { |e, index| PayeerAccount.new(e.merge(id: index)) }
    end

    def all_accounts_balance
      payeer_accounts.map(&:balance)
                     .reduce(&:+)
                     .round(2)
    end
  end
end
