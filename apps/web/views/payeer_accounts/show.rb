module Web::Views::PayeerAccounts
  class Show
    include Web::View

    COLLECTIVE_ACCOUNT_ID = 1000
    DATE_WITHOUT_TIMEZONE_FORMAT = '%F %R'.freeze
    ARCHIVED_PERIOD = :archived

    def account
      account_id = params[:id].to_i

      accounts = ConfigHelper.section_in_symbols(:payeer_accounts)

      if account_id == COLLECTIVE_ACCOUNT_ID
        PayeerAccountCollection.new(accounts)
      else
        PayeerAccount.new(accounts[account_id])
      end
    end

    def balance
      account.balance
             .round(2)
    end

    def operations
      if params[:period] == ARCHIVED_PERIOD.to_s
        account.archived_operations
      else
        new_operations
      end
    end

    def new_operations
      period_hash = period_procs.fetch(params[:period] || 'day').call

      args = {
        from: period_hash.fetch(:from),
        to: period_hash.fetch(:to),
        type: params[:type]
      }

      args.delete_if { |_key, val| val.nil? }

      account.operations(args)
    end

    def outcome_operations
      internal_account_ids = ConfigHelper.section_in_symbols(:payeer_accounts)
                                         .map(&PayeerAccount.method(:new))
                                         .map(&:account_id)

      grouped_operations.select { |(account_id, *)| internal_account_ids.include?(account_id) }
    end

    def outcome_other_operations
      outcome_operations.map do |(account_id, local_operations)|
        filtered_operations = local_operations.reject(&method(:transfer_operation?))
                                              .reject(&method(:sci_operation?))
        [account_id, filtered_operations]
      end
    end

    def income_operations
      grouped_operations - outcome_operations
    end

    def outcome_transfer_operations
      select_operation_groups(outcome_operations, &method(:transfer_operation?))
    end

    def outcome_exchange_operations
      select_operation_groups(
        select_operation_groups(outcome_operations,
                                &method(:sci_operation?)),
        &method(:exchange_operation?)
      )
    end

    def outcome_sci_operations
      reject_operation_groups(
        select_operation_groups(outcome_operations,
                                &method(:sci_operation?)),
        &method(:exchange_operation?)
      )
    end

    def operations_grouping(operations)
      grouping_proc = grouping_procs.fetch(params[:grouping] || 'day')

      operations.group_by(&grouping_proc)
                .to_a
    end

    def operations_sum(operations)
      if operations.any?
        operations.map(&:amount)
                  .map(&:to_f)
                  .reduce(&:+)
                  .round(2)
      else
        0
      end
    end

    def account_brand(account_id)
      PayeerAccount.new(account_id: account_id).brand
    end

    private

    def select_operation_groups(local_operation_groups, &filter_proc)
      local_operation_groups.map do |(account_id, local_operations)|
        selected_operations = local_operations.select(&filter_proc)
        [account_id, selected_operations] if selected_operations.any?
      end.compact
    end

    def reject_operation_groups(local_operation_groups, &filter_proc)
      local_operation_groups.map do |(account_id, local_operations)|
        selected_operations = local_operations.reject(&filter_proc)
        [account_id, selected_operations] if selected_operations.any?
      end.compact
    end

    def sci_operation?(operation)
      operation.is_a?(PayeerOperationSci)
    end

    def transfer_operation?(operation)
      operation.is_a?(PayeerOperationTransfer)
    end

    def exchange_operation?(operation)
      ConfigHelper.config[:exchangers].include?(operation.shop_domain)
    end

    def grouping_procs
      dates_to_str = lambda do |*dates|
        dates.map do |d|
          d.strftime(DATE_WITHOUT_TIMEZONE_FORMAT)
        end.join(' - ')
      end

      {
        'day' => -> (e) { dates_to_str[e.date.beginning_of_day, e.date.end_of_day] },
        'week' => -> (e) { dates_to_str[e.date.beginning_of_week, e.date.end_of_week] },
        'month' => -> (e) { dates_to_str[e.date.beginning_of_month, e.date.end_of_month] }
      }
    end

    def period_procs
      current_time = Time.now.to_date

      {
        'day' => -> { { from: current_time, to: nil } },
        'prev_day' => -> { { from: current_time - 1.day, to: (current_time - 1.day).end_of_day } },
        'week' => -> { { from: current_time - 1.week, to: nil } },
        'month' => -> { { from: current_time - 30.days, to: nil } },
        'calendar_month' => -> { { from: current_time.beginning_of_month, to: nil } }
      }
    end

    def grouped_operations
      # @grouped_operations ||= operations.group_by(&:grouping_field)
      @grouped_operations ||= operations.group_by(&:from)
                                        .to_a
    end
  end
end
