module Web::Controllers::PayeerAccounts
  class Show
    include Web::Action

    params do
      required(:id).filled(:int?)
      optional(:period).filled(:str?)
      optional(:grouping).filled(:str?)
      optional(:type).filled(:str?)
    end

    def call(params)
    end
  end
end
