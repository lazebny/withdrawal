require_relative '../helpers/debug_helper'

module Withdrawal
  module Services
    class StortiInvestmentWithdrawal
      include ::Capybara::DSL
      include Withdrawal::Helpers::DebugHelper

      LOGIN_URL = 'https://storti-investments.com'.freeze
      WITHDRAW_URL = 'https://storti-investments.com/cabinet/balance/withdraw/new/#scroll'.freeze
      MIN_SUM = 0
      BASE_DELAY = 1

      def initialize(login:, password:)
        @login = login
        @password = password
        @payeer_account_id = ConfigHelper.section_in_symbols(:payeer_accounts)
                                         .first
                                         .fetch(:account_id)
      end

      def call
        login

        withdraw

        Capybara.reset_sessions!
      end

      private

      # ACTIONS ---------------------------------------------------------------
      def login
        visit LOGIN_URL
        long_sleep 1
        # long_sleep 15
        # within('#authorization') do
        click_on('Sign In')
        # first('.account_link').click
        # end
        shot('01_before_login')

        long_sleep 1
        within('.arcticmodal-container form') do
          fill_in('user_login', with: @login)
          fill_in('user_pass', with: @password)
          click_button('Sign In')
          # find("button[type=submit]").click
        end
        shot('02_after_login')
      end

      def withdraw
        long_sleep 1

        visit WITHDRAW_URL
        shot('03_before_form')

        within('form') do
          profit_float = extract_profit(find('span').text)

          return if profit_float <= MIN_SUM

          fill_in('pr_sum', with: profit_float)
          select('Payeer', from: 'type_select')
          fill_in('pr_address', with: @payeer_account_id)
          first(' button[type=submit]').click
        end

        long_sleep 5
        shot('04_after_withdraw')
      end

      def extract_profit(profit_str)
        debug(profit_str: profit_str)

        profit_float = profit_str.to_f
        debug(clear_profit: profit_float)

        profit_float
      end

      def long_sleep(time)
        sleep BASE_DELAY * time
      end

      def shot(filename = 'capybara')
        msg = [
          'storti_investment',
          @login,
          filename
        ].join('-')
        super(msg)
      end
    end
  end
end
