require_relative '../helpers/debug_helper'

module Withdrawal
  module Services
    class RosoplataWithdrawalV1
      include ::Capybara::DSL
      include Withdrawal::Helpers::DebugHelper

      LOGIN_URL = 'https://rosoplata.com/site/login'.freeze
      # DASBOADRD_URL = ''
      PAYEER_KEY = '1'.freeze
      MAX_WITHDRAW_SUM = 5.0

      def initialize(login:, password:)
        @login = login
        @password = password
      end

      def call
        login

        withdraw

        Capybara.reset_sessions!
      end

      private

      # ACTIONS ---------------------------------------------------------------
      def login
        visit LOGIN_URL
        sleep 15

        within('#login-form') do
          fill_in('LoginForm[email]', with: @login)
          fill_in('LoginForm[password]', with: @password)
          find('button[type=submit]').click
        end
      end

      def withdraw
        sleep 15

        profit_str = find('.yourMoney span', text: 'USD').text
        profit_float = extract_profit(profit_str)
        partial_withdraw(extract_profit(profit_str)) if profit_float > 0
      end

      def partial_withdraw(value)
        sum = value > MAX_WITHDRAW_SUM ? MAX_WITHDRAW_SUM : value

        first('input#cashoutform-psys', visible: false).set(PAYEER_KEY)
        first('input#cashoutform-sum', visible: false).set(sum)

        sleep 15

        first('.insm_out button[type=submit]', visible: false).trigger('click')

        sleep 30

        partial_withdraw(value - MAX_WITHDRAW_SUM) if value > MAX_WITHDRAW_SUM
      end

      def extract_profit(profit_str)
        debug(profit_str: profit_str)

        profit_float = profit_str.to_f
        debug(clear_profit: profit_float)

        profit_float
      end
    end
  end
end
