require_relative '../helpers/debug_helper'

module Withdrawal
  module Services
    class GreenTelecomWithdrawal
      include Capybara::DSL
      include Withdrawal::Helpers::DebugHelper

      LOGIN_URL = 'https://telecomfin.info/personal/?idioma=en'.freeze
      DASBOADRD_URL = 'https://telecomfin.info/personal'.freeze
      VIP_CARDS_URL = 'https://telecomfin.info/personal/sattelite'.freeze
      MIN_WIDTHDRAW = 1.0

      def initialize(login:, password:)
        @login = login
        @password = password
      end

      def call
        login

        withdraw_dashboard_cards
        withdraw_vip_cards

        Capybara.reset_sessions!
      end

      private

      # ACTIONS ---------------------------------------------------------------
      def login
        visit LOGIN_URL

        within('#pane-login') do
          fill_in('login', with: @login)
          fill_in('password', with: @password)
          click_button('Enter')
        end
      end

      # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      def withdraw_dashboard_cards
        shot('dashboard_contracts')
        contract_ids = all('.card-meal').map(&method(:extract_contract_id))
        contract_ids.each_with_index do |contract_id, index|
          debug(contract_id: contract_id)

          visit(DASBOADRD_URL) if index > 0

          all('.card-meal').each_with_index do |card, _index|
            next unless contract_id == extract_contract_id(card)

            profit_float = extract_profit(card: card)

            next unless enough_money?(profit_float)

            debug('before withdrawal')

            card.trigger('mousemove')

            withdraw_value(card: card,
                           value: profit_float)

            sleep 5
            shot(contract_id)
          end
        end
      end
      # rubocop:enable Metrics/MethodLength, Metrics/AbcSize

      # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      def withdraw_vip_cards
        visit(VIP_CARDS_URL)
        shot('vip_contracts')

        contract_ids = all('.card-contract').map(&method(:extract_contract_id))
        contract_ids.each_with_index do |contract_id, index|
          debug(contract_id: contract_id)

          visit(VIP_CARDS_URL) if index > 0

          all('.card-contract').each do |card|
            next unless contract_id == extract_contract_id(card)

            profit_float = extract_profit(card: card)

            next unless enough_money?(profit_float)

            withdraw_value(card: card,
                           value: profit_float)
            sleep 5
            shot(contract_id)
          end
        end
      end
      # rubocop:enable Metrics/MethodLength, Metrics/AbcSize

      # HELPERS ---------------------------------------------------------------
      def enough_money?(amount)
        amount >= MIN_WIDTHDRAW
      end

      def extract_profit(card:)
        profit_str = card.find('.prof')['data-val']
        debug(raw_profit: profit_str)

        profit_float = (profit_str.to_f.round(2) - 0.01).round(2)
        debug(clear_profit: profit_float)

        profit_float
      end

      def withdraw_value(card:, value:)
        card.find('button', text: 'Withdrawal').trigger('click')

        within('#sellForm') do
          fill_in('value', with: value)
          click_button('Withdraw the amount entered')
        end
      end

      def extract_contract_id(card)
        head = card.find('.card-heading').text
        debug(head)
        head.split(' ').first
      end

      def shot(filename = 'capybara')
        msg = [
          'green_telecom',
          @login.split('@').first,
          filename
        ].join('-')
        super(msg)
      end
    end
  end
end
