require_relative '../helpers/debug_helper'

module Withdrawal
  module Services
    class RosoplataWithdrawalV2
      include ::Capybara::DSL
      include Withdrawal::Helpers::DebugHelper

      LOGIN_URL = 'https://rosoplata.com/site/login'.freeze
      # DASBOADRD_URL = ''
      WITHDRAW_KEY = '20'.freeze
      MAX_WITHDRAW_SUM = 5.0
      BASE_DELAY = 2

      def initialize(login:, password:)
        @login = login
        @password = password
      end

      def call
        login

        withdraw

        Capybara.reset_sessions!
      end

      private

      # ACTIONS ---------------------------------------------------------------
      def login
        visit LOGIN_URL
        long_sleep 15
        shot('01_before_login')

        within('#login-form') do
          fill_in('LoginForm[email]', with: @login)
          fill_in('LoginForm[password]', with: @password)
          find('button[type=submit]').click
        end
        shot('02_after_login')
      end

      def withdraw
        long_sleep 15
        shot('03_before_withdraw')

        profit_str = find('.yourMoney span', text: 'USD').text
        profit_float = extract_profit(profit_str)

        first('input#cashoutform-psys', visible: false).set(WITHDRAW_KEY)
        first('input#cashoutform-sum', visible: false).set(profit_float)

        long_sleep 15
        shot('03_before_withdraw_submit')

        first('.insm_out button[type=submit]', visible: false).trigger('click')
        shot('03_after_withdraw')
      end

      def extract_profit(profit_str)
        debug(profit_str: profit_str)

        profit_float = profit_str.to_f
        debug(clear_profit: profit_float)

        profit_float
      end

      def long_sleep(time)
        sleep BASE_DELAY * time
      end

      def shot(filename = 'capybara')
        msg = [
          'rosoplata',
          @login.split('@').first,
          filename
        ].join('-')
        super(msg)
      end
    end
  end
end
