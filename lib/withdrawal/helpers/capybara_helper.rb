require 'capybara/poltergeist'

module Withdrawal
  module Helpers
    module CapybaraHelper
      module_function

      def setup(use_proxy: true)
        Capybara.default_driver = :poltergeist
        Capybara.javascript_driver = :poltergeist
        Capybara.register_driver :poltergeist do |app|
          phantomjs_options = [
            '--load-images=no',
            '--ignore-ssl-errors=yes',
            '--ssl-protocol=any',
            '--web-security=true',
            '--debug=true'
          ]
          if use_proxy
            phantomjs_options += [
              "--proxy=#{Withdrawal::Helpers::ProxyHelper.find.join(':')}",
              '--proxy-type=http'
            ]
          end

          options = {
            js_errors: false,
            debug: true,
            timeout: 600,
            screen_size: [1600, 1200],
            window_size: [1600, 1200],
            phantomjs_options: phantomjs_options
          }
          Capybara::Poltergeist::Driver.new(app, options)
          # Capybara.current_session.driver.header('User-Agent' => 'iPhone')
        end
      end

      # Capybara.javascript_driver = :webkit
      # Capybara.javascript_driver = :selenium
      # Capybara.javascript_driver = :webkit_debug

      # Capybara.register_driver :chrome do |app|
      #   Capybara::Selenium::Driver.new(app,
      #                                  browser: :chrome,
      #                                  desired_capabilities: {
      #                                    'chromeOptions' => {
      #                                      'args' => %w( window-size=1024,768 )
      #                                    }
      #                                  },
      #                                 # debug: true,
      #                                 # js_errors: true
      #                                 )
      # end

      # Capybara::Webkit.configure(&:block_unknown_urls)
      # Capybara::Webkit.configure do |config|
      #   config.allow_url('*')
      #   config.timeout = 300
      # end
    end
  end
end
