require 'csv'

module Withdrawal
  module Helpers
    module ProxyHelper
      module_function

      def find
        loop do
          proxy = GimmeProxy.new

          proxy_data = [proxy.ip, proxy.port]
          return proxy_data if active_proxy?(proxy_data)
        end
      end

      private_class_method

      def active_proxy?((host, port))
        Net::Ping::TCP.new(host, port.to_i).ping?
      end

      def proxy_list
        # CSV.parse(File.read('./spec/proxy'), col_sep: ' ')
        [
          ['85.43.201.190', '3128']
        ]
      end

      class GimmeProxy
        include Withdrawal::Helpers::DebugHelper

        PROXY_URL = 'http://gimmeproxy.com/api/getProxy?' + [
          'supportsHttps=true',
          'protocol=http',
          'anonymityLevel=1',
          'referer=true',
          'cookies=true'
        ].join('&')

        def initialize
          response = Typhoeus.get(PROXY_URL)

          raise %(Not expected response code: #{response.code}) if response.code >= 300

          @json = JSON.parse(response.body)

          debug(proxy_search: @json)
        end

        def ip
          @json.fetch('ip')
        end

        def port
          @json.fetch('port')
        end
      end
    end
  end
end
