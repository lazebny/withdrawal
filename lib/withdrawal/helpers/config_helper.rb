module ConfigHelper
  module_function

  def config
    symbolize_hash(YAML.load(File.read('./config/config.yml')))
  end

  def symbolize_hash(hash)
    hash.keys.each_with_object({}) { |e, a| a[e.to_sym] = hash.fetch(e) }
  end

  def section_in_symbols(section_name)
    case section_config = config.fetch(section_name)
    when Hash
      symbolize_hash(section_config)
    when Array
      section_config.map(&method(:symbolize_hash))
    else
      section_config
    end
  end
end
