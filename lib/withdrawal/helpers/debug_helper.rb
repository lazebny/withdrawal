module Withdrawal
  module Helpers
    module DebugHelper
      def shot(name = 'capybara')
        page.driver.save_screenshot("./tmp/capybara/#{name}.png")
      end

      def debug(value)
        puts ['=========================================================',
              "message: #{value}",
              '========================================================='].join("\n")
      end
    end
  end
end
