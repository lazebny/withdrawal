module Withdrawal
  module Workers
    class RosoplataWorker
      def self.perform
        accounts = ConfigHelper.section_in_symbols(:rosoplata_credentials)
        accounts
          .map(&Withdrawal::Services::RosoplataWithdrawalV2.method(:new))
          .map(&:call)
      end
    end
  end
end
