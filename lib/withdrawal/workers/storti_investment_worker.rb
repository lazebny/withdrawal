module Withdrawal
  module Workers
    class StortiInvestmentWorker
      def self.perform
        accounts = ConfigHelper.section_in_symbols(:storti_investment_credentials)
        accounts
          .map(&Withdrawal::Services::StortiInvestmentWithdrawal.method(:new))
          .map(&:call)
      end
    end
  end
end
