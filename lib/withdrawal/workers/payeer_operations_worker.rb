module Withdrawal
  module Workers
    class BackupPayeerOperationsWorker
      def self.perform
        accounts = ConfigHelper.section_in_symbols(:payeer_accounts)

        operations = PayeerAccountCollection
                     .new(accounts)
                     .operations
                     .map(&PayeerOperationRepository.method(:persist))
      end
    end
  end
end
