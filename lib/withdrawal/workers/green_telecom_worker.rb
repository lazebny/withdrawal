module Withdrawal
  module Workers
    class GreenTelecomWorker
      def self.perform
        accounts = ConfigHelper.section_in_symbols(:green_telecom_credentials)
        accounts
          .map(&Withdrawal::Services::GreenTelecomWithdrawal.method(:new))
          .map(&:call)
      end
    end
  end
end
