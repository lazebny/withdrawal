class PayeerAccountCollection
  include Hanami::Entity

  def initialize(accounts)
    @accounts = accounts.map(&PayeerAccount.method(:new))
  end

  def email
    @accounts.map(&:email)
  end

  def account_id
    @accounts.map(&:account_id)
  end

  def operations(**args)
    @accounts
      .flat_map { |e| e.operations(args) }
      .sort_by(&:date)
      .reverse
  end

  def archived_operations
    PayeerOperationRepository
      .all
      .map(&:load_source)
      .map(&PayeerOperation.method(:build))
  end

  def balance
    @accounts.map(&:balance)
             .reduce(&:+)
             .round(2)
  end

  def to_s
    account_infos = email.zip(account_id)
                         .map { |(a_email, a_id)| %(#{a_email} (#{a_id})) }

    [account_infos.join(', '), "balance: #{balance}$"].join("\n")
  end
end
