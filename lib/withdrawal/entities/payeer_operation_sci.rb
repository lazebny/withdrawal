require_relative 'payeer_operation'

class PayeerOperationSci < PayeerOperation
  attributes :shop

  def shop_domain
    shop.fetch('domain')
  end

  alias to shop_domain
  alias grouping_field to
end
#  {:id=>"222069263",
#    :date=>"2016-08-18 22:37:49",
#    :type=>"sci",
#    :status=>"success",
#    :from=>"P40115388",
#    :debitedAmount=>"60.58",
#    :debitedCurrency=>"USD",
#    :creditedAmount=>"60.58",
#    :creditedCurrency=>"USD",
#    :exchangeRate=>1,
#    :shop=>{"id"=>"211710777",
#            "domain"=>"quickpay.today",
#            "orderid"=>"143076",
#            "description"=>"60.00 23572"}},
#
#  {:id=>"215964727",
#    :date=>"2016-08-03 08:00:02",
#    :type=>"sci",
#    :status=>"success",
#    :from=>"P40115388",
#    :debitedAmount=>"50.00",
#    :debitedCurrency=>"USD",
#    :creditedAmount=>"50.00",
#    :creditedCurrency=>"USD",
#    :exchangeRate=>1,
#    :shop=>{"id"=>"132531196",
#            "domain"=>"rosoplata.com",
#            "orderid"=>"248938",
#            "description"=>"cash in #14717"}}}
