class PayeerOperation
  include Hanami::Entity

  attributes :id,
             :date,
             :type,
             :status,
             :from,
             :to,
             :amount,
             :currency,
             :shop,
             :source

  def self.build(**attrs)
    operation_class = {
      'sci' => PayeerOperationSci,
      'transfer' => PayeerOperationTransfer
    }.fetch(attrs.fetch(:type), self)

    operation_class.new(attrs)
  end

  # type:
  # transfer - perevod
  # deposit - popolnenie
  # withdrawal - vivod
  # sci - oplata v magazin

  def initialize(**attrs)
    unless attrs[:source].nil?
      attrs = ConfigHelper.symbolize_hash(JSON.load(attrs.fetch(:source)))
    end

    attrs[:amount] = attrs.fetch(:creditedAmount)
    attrs[:currency] = attrs.fetch(:creditedCurrency)
    attrs[:date] = Time.parse(attrs.fetch(:date))
    attrs[:source] = JSON.dump(attrs)

    super(attrs)
  rescue StandardError => e
  end

  def load_source
    ConfigHelper.symbolize_hash(JSON.load(source))
  end

  # def from_record(record)
  #   self.class.new(record.source)
  # end

  alias grouping_field from
end
#  {:id=>"222069263",
#    :date=>"2016-08-18 22:37:49",
#    :type=>"sci",
#    :status=>"success",
#    :from=>"P40115388",
#    :debitedAmount=>"60.58",
#    :debitedCurrency=>"USD",
#    :creditedAmount=>"60.58",
#    :creditedCurrency=>"USD",
#    :exchangeRate=>1,
#    :shop=>{"id"=>"211710777",
#            "domain"=>"quickpay.today",
#            "orderid"=>"143076",
#            "description"=>"60.00 23572"}},
#
#  {:id=>"215964727",
#    :date=>"2016-08-03 08:00:02",
#    :type=>"sci",
#    :status=>"success",
#    :from=>"P40115388",
#    :debitedAmount=>"50.00",
#    :debitedCurrency=>"USD",
#    :creditedAmount=>"50.00",
#    :creditedCurrency=>"USD",
#    :exchangeRate=>1,
#    :shop=>{"id"=>"132531196",
#            "domain"=>"rosoplata.com",
#            "orderid"=>"248938",
#            "description"=>"cash in #14717"}}}
