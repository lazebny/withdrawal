class PayeerAccount
  include Hanami::Entity

  UNKNOWN_ACCOUNT = 'Unknown'.freeze
  MAX_PAYEER_HISTORY_COUNT = 1000

  attributes :email, :account_id, :api_id, :api_key

  # rubocop:disable Metrics/MethodLength
  def operations(from: nil, to: nil, count: MAX_PAYEER_HISTORY_COUNT, type: nil)
    args = {
      action: 'history',
      from: from.nil? ? nil : from.strftime('%F %R'),
      to: to.nil? ? nil : to.strftime('%F %R'),
      # to: Time.now.strftime("%F %R"),
      type: type,
      count: count
    }

    args.delete_if { |*, val| val.nil? }

    operation_hashes = payeer_client.api_call(args)
                                    .fetch('history')
                                    .to_a
                                    .sort_by(&:first)
                                    .reverse
                                    .map(&:last)
                                    .map(&ConfigHelper.method(:symbolize_hash))

    operation_hashes.map(&PayeerOperation.method(:build))
  end
  # rubocop:enable Metrics/MethodLength

  def archived_operations
    raise 'Please chose all accounts.'
  end

  def brand
    account = ConfigHelper.section_in_symbols(:payeer_payers)
                          .find { |e| e.fetch(:account_ids).include?(account_id) }
    account ? account.fetch(:name) : UNKNOWN_ACCOUNT
  end

  def balance
    payeer_client
      .balance
      .fetch('balance')
      .fetch('USD')
      .fetch('DOSTUPNO')
      .to_f
      .round(2)
  end

  def to_s
    %(#{email} (#{account_id}), balance: #{balance}$)
  end

  private

  def payeer_client
    @client ||= Payeer.new(account_id, api_id, api_key)
  end
end
