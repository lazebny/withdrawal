require 'hanami/model'
require 'hanami/mailer'
Dir["#{__dir__}/withdrawal/**/*.rb"].each { |file| require_relative file }

Hanami::Model.configure do
  ##
  # Database adapter
  #
  # Available options:
  #
  #  * File System adapter
  #    adapter type: :file_system, uri: 'file:///db/bookshelf_development'
  #
  #  * Memory adapter
  #    adapter type: :memory, uri: 'memory://localhost/withdrawal_development'
  #
  #  * SQL adapter
  #    adapter type: :sql, uri: 'sqlite://db/withdrawal_development.sqlite3'
  #    adapter type: :sql, uri: 'postgres://localhost/withdrawal_development'
  #    adapter type: :sql, uri: 'mysql://localhost/withdrawal_development'
  #
  # adapter type: :sql, uri: ENV['DATABASE_URL']
  # adapter type: :sql, uri: 'sqlite://db/withdrawal_development.sqlite3'
  adapter type: :file_system, uri: ENV['DATABASE_URL']

  ##
  # Database mapping
  #
  # Intended for specifying application wide mappings.
  #
  mapping do
    collection :payeer_accounts do
      entity     PayeerAccount
      repository PayeerAccountRepository

      attribute :id, Integer
      attribute :email, String
      attribute :account_id, String
      attribute :api_id, String
      attribute :api_key, String
    end

    collection :payeer_operations do
      entity     PayeerOperation
      repository PayeerOperationRepository

      attribute :id, Integer
      attribute :source, String
    end
  end
end.load!

Hanami::Mailer.configure do
  root "#{__dir__}/withdrawal/mailers"

  # See http://hanamirb.org/guides/mailers/delivery
  delivery do
    development :test
    test        :test
    # production :smtp, address: ENV['SMTP_PORT'], port: 1025
  end
end.load!
