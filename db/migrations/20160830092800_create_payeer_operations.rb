Hanami::Model.migration do
  change do
    create_table :payeer_operations do
      primary_key :id

      column :date, String
      column :type, String
      column :status, String
      column :from, String
      column :debitedAmount, Float
      column :debitedCurrency, String
      column :creditAmount, Float
      column :creditCurrency, String
      column :exchangeRate, Integer
      column :shopId, String
      column :shopDomain, String
      column :shopOrderId, String
      column :shopDescription, String
    end
  end
end
